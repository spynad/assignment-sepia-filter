// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#include "transform_rotate.h"

struct image * rotate(struct image const source ) {
    struct image* new_image = image_init(source.height, source.width);
    int32_t j = source.width - 1;
    int32_t i = 0;
    while (true) {
        for (int32_t k = 0; k < source.height; ++k) {
            struct pixel pixel = {0};
            image_get_pixel_by_coordinates(&source, k, j, &pixel);
            array_array_pixel_set(*new_image->data, i, k, pixel);
        }

        if (j == 0 || i == source.width - 1) {
            break;
        }
        i++;
        j--;
    }

    return new_image;
}
