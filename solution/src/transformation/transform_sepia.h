//
// Created by spynad on 08.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_SEPIA_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_SEPIA_H
#include <inttypes.h>
#include "../image/pixel.h"
#include "../image/image.h"

struct image* apply_sepia(struct image* img);
struct image* apply_sepia_asm(struct image* img);
void test_sepia_asm_c();

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_SEPIA_H
