// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#ifndef BMP_IMAGE_ROTATOR_TRANSFORM_ROTATE_H
#define BMP_IMAGE_ROTATOR_TRANSFORM_ROTATE_H

#include "../image/image.h"

struct image * rotate(struct image const source );

#endif //BMP_IMAGE_ROTATOR_TRANSFORM_ROTATE_H
