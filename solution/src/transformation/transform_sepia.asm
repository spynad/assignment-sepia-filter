global sepia_sse
section .rodata

align 16
max:
    dd 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF

align 16
sepia_coef_1_1:
    dd 0.189, 0.168, 0.131, 0.189

align 16
sepia_coef_1_2:
    dd 0.769, 0.686, 0.543, 0.769

align 16
sepia_coef_1_3:
    dd 0.393, 0.349, 0.272, 0.393

align 16
sepia_coef_2_1:
    dd 0.168, 0.131, 0.189, 0.168

align 16
sepia_coef_2_2:
    dd 0.686, 0.543, 0.769, 0.686

align 16
sepia_coef_2_3:
    dd 0.349, 0.272, 0.393, 0.349

align 16
sepia_coef_3_1:
    dd 0.131, 0.189, 0.168, 0.131

align 16
sepia_coef_3_2:
    dd 0.543, 0.769, 0.686, 0.543

align 16
sepia_coef_3_3:
    dd 0.272, 0.393, 0.349, 0.272

section .text

;rdi - struct pixel_float[4]
;rsi - struct pixel_float[4]
sepia_sse:
    mov r8, rsp
    sub rsp, 16
    and rsp, -16
    ;cvtdq2ps xmm0, [rdi]
    mov rax, [rdi]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov rax, [rdi + 12]
    mov [rsp + 12], eax
    movaps xmm0, [rsp]

    mov rax, [rdi + 4]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov rax, [rdi + 16]
    mov [rsp + 12], eax
    movaps xmm1, [rsp]

    mov rax, [rdi + 8]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov rax, [rdi + 20]
    mov [rsp + 12], eax
    movaps xmm2, [rsp]

    mulps xmm0, [sepia_coef_1_1]
    mulps xmm1, [sepia_coef_1_2]
    mulps xmm2, [sepia_coef_1_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvttps2dq xmm0, xmm0
    pminsd xmm0, [max]
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    pextrb byte[rsi], xmm0, 14
    pextrb byte[rsi + 1], xmm0, 13
    pextrb byte[rsi + 2], xmm0, 12
    pextrb byte[rsi + 5], xmm0, 15
    ;movaps [rsi], xmm0
;----------------------
    mov rax, [rdi + 12]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov rax, [rdi + 24]
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm0, [rsp]

    mov rax, [rdi + 16]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov rax, [rdi + 28]
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm1, [rsp]

    mov rax, [rdi + 20]
    mov [rsp], eax
    mov [rsp + 4], eax
    mov rax, [rdi + 32]
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm2, [rsp]

    ; mulps xmm0, [sepia_coef + 4]
    ; mulps xmm1, [sepia_coef + 36]
    ; mulps xmm2, [sepia_coef + 70]
    mulps xmm0, [sepia_coef_2_1]
    mulps xmm1, [sepia_coef_2_2]
    mulps xmm2, [sepia_coef_2_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvttps2dq xmm0, xmm0
    pminsd xmm0, [max]
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    pextrb byte[rsi + 3], xmm0, 13
    pextrb byte[rsi + 4], xmm0, 12
    pextrb byte[rsi + 7], xmm0, 15
    pextrb byte[rsi + 8], xmm0, 14

    mov rax, [rdi + 24]
    mov [rsp], eax
    mov rax, [rdi + 36]
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm0, [rsp]

    mov rax, [rdi + 28]
    mov [rsp], eax
    mov rax, [rdi + 40]
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm1, [rsp]

    mov rax, [rdi + 32]
    mov [rsp], eax
    mov rax, [rdi + 44]
    mov [rsp + 4], eax
    mov [rsp + 8], eax
    mov [rsp + 12], eax
    movaps xmm2, [rsp]

    ; mulps xmm0, [sepia_coef + 8]
    ; mulps xmm1, [sepia_coef + 40]
    ; mulps xmm2, [sepia_coef + 70]
    mulps xmm0, [sepia_coef_3_1]
    mulps xmm1, [sepia_coef_3_2]
    mulps xmm2, [sepia_coef_3_3]

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvttps2dq xmm0, xmm0
    pminsd xmm0, [max]
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    pextrb byte[rsi + 6], xmm0, 12
    pextrb byte[rsi + 9], xmm0, 15
    pextrb byte[rsi + 10], xmm0, 14
    pextrb byte[rsi + 11], xmm0, 13

    ;lea rax, [rel .sepia_coef]
    ;mulps xmm0, [rax]
    ;addps xmm0, [rsi]
    ;movdqa [rdi], xmm0
    mov rsp, r8
    ret

