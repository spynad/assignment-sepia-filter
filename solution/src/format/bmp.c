// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#include "bmp.h"

static bool bmp_check_signature (const struct bmp_header* header) {
    unsigned char bytes[2];
    bytes[0] = header->bfSignature & 0xFF;
    bytes[1] = (header->bfSignature >> 8) & 0xFF;
    if (bytes[0] == 'B' && bytes[1] == 'M') {
        return true;
    }

    //debug_print("Invalid signature: %c%c", bytes[0], bytes[1]);
    return false;
}

static bool bmp_to_image (FILE* file, const struct bmp_header* header, struct image** img) {
    *img = image_init(header->biWidth, header->biHeight);
    for (int32_t i = 0; i < header->biHeight; ++i) {
        for (int32_t j = 0; j < header->biWidth; ++j) {
            struct pixel pixel = {0};
            if (!fread(&pixel, sizeof (struct pixel), 1, file)) {
                //debug_print("error while parsing bmp pixel data on x:%zu y:%zu", i, j);
                return false;
            }
            image_set_pixel_by_coordinates(*img, header->biHeight - i - 1, j, &pixel);
        }
        if (fseek(file,
                  ((header->biWidth * sizeof (struct pixel)) % 4) ? 4 - (header->biWidth * sizeof (struct pixel)) % 4 : 0,
                          SEEK_CUR)) return false;
    }

    return true;
}

static struct bmp_header bmp_construct_header (struct image const* img) {
    struct bmp_header header = {0};
    uint32_t fileSize   = sizeof (struct bmp_header)
                          + sizeof (struct pixel) * (img->height * img->width)
                          + ((img->width % 4) ? (img->height + 1) * (4 - (img->width % 4)) : 0);
    header.bfSignature  = ('M' << 8) + 'B';
    header.bOffBits     = 54;
    header.biSize       = 40;
    header.biWidth      = img->width;
    header.biHeight     = img->height;
    header.biPlanes     = 1;
    header.biBitCount   = 24;
    header.bfileSize    = fileSize;
    header.biSizeImage  = fileSize - sizeof (struct bmp_header);

    return header;
}

enum read_status from_bmp (FILE* in, struct image** img) {
    struct bmp_header header = {0};
    if (in) {
        if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
            //debug_print("Failed to read bmp header from file &p", in);
            return READ_INVALID_HEADER;
        }

        if (!bmp_check_signature(&header)) {
            return READ_INVALID_SIGNATURE;
        }

        if (header.biWidth <= 0 || header.biHeight <= 0) {
            return READ_INVALID_BITS;
        }

        if (!bmp_to_image(in, &header, img)) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp (FILE* out, struct image const* img) {
    struct bmp_header header = bmp_construct_header(img);

    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) {
        //debug_print("Unable to write a bmp header to a file");
        return WRITE_ERROR;
    }

    for (int32_t i = 0; i < header.biHeight; ++i) {
        for (int32_t j = 0; j < header.biWidth; ++j) {
            struct pixel pixel = {0};
            if (image_get_pixel_by_coordinates(img, header.biHeight - i - 1, j, &pixel) == IMAGE_OK) {
                if (!fwrite(&pixel, sizeof (struct pixel), 1, out)) {
                    //debug_print("Unable to write a pixel");
                    return WRITE_ERROR;
                }
            } else return WRITE_ERROR;
        }
        int8_t zero = 0;
        if ((header.biWidth * 3) % 4 != 0) {
            for (size_t k = 0; k < 4 - (header.biWidth * 3) % 4; ++k)
                if (!fwrite(&zero, sizeof (int8_t), 1, out)) return WRITE_ERROR;
        }

    }

    return WRITE_OK;
}

