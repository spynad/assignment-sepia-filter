section text
global test_sse


.sepia_coef:
    dd 0.393, 0.769, 0.189
    dd 0.349, 0.686, 0.168
    dd 0.272, 0.543, 0.131

;rdi - pixel
;rsi - something
test_sse:
    movdqa xmm0, [rdi]
    mulps xmm0, [rsi]
    addps xmm0, [rsi]
    movdqa [rdi], xmm0
    ret

