// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#ifndef BMP_IMAGE_ROTATOR_MARRAY_PIXEL_H
#define BMP_IMAGE_ROTATOR_MARRAY_PIXEL_H

#include "array_pixel.h"
#include "stdbool.h"
#include "stdint.h"

struct array_array_pixel {
    struct array_pixel* data;
    size_t size;
};

struct optional_array_array_pixel {
    struct array_array_pixel* value;
    bool valid;
};

extern const struct optional_array_array_pixel NONE_ARRAY_ARRAY_PIXEL;

struct optional_array_array_pixel array_array_pixel_init(size_t w, size_t h);
struct optional_pixel array_array_pixel_get(struct array_array_pixel a, size_t i, size_t j);
bool array_array_pixel_set(struct array_array_pixel a, size_t i, size_t j, struct pixel value);
void array_array_pixel_free(struct array_array_pixel* array);

struct optional_array_array_pixel some_array_array_pixel(struct array_array_pixel* array);

#endif //BMP_IMAGE_ROTATOR_MARRAY_PIXEL_H
