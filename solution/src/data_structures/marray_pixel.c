// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#include "marray_pixel.h"
#include "../image/pixel.h"
#include "malloc.h"

const struct optional_array_array_pixel NONE_ARRAY_ARRAY_PIXEL = {NULL, false };

struct optional_array_array_pixel array_array_pixel_init (size_t w, size_t h) {
    //debug_print("Allocating memory for pixel array with size %zux%zu\n", w, h);
    struct array_array_pixel* marray = malloc(sizeof (struct array_array_pixel));
    if (marray) {
        //debug_print("Allocated successfully at %p\n", marray);
        marray->data = malloc(sizeof(struct array_pixel) * w);
        if (marray->data) {
            for (size_t i = 0; i < w; ++i) {
                struct optional_array_pixel opt_pixel = array_pixel_init(h);
                if (opt_pixel.valid) {
                    marray->data[i] = opt_pixel.value;
                    marray->size = h;
                } else {
                    for (size_t j = 0; j < i - 1; ++j) {
                        array_pixel_free(&marray->data[i]);
                    }
                    free(marray->data);
                    free(marray);
                    //debug_print("Malloc failure\n");
                    return NONE_ARRAY_ARRAY_PIXEL;
                }
            }

            marray->size = w;
            return some_array_array_pixel(marray);
        } else {
            free(marray);
            //debug_print("Malloc failure\n");
            return NONE_ARRAY_ARRAY_PIXEL;
        }
    }

    //debug_print("Malloc failure\n");
    return NONE_ARRAY_ARRAY_PIXEL;
}

struct optional_pixel array_array_pixel_get (struct array_array_pixel a, size_t i, size_t j ) {
    if (!a.data || a.size == 0 || i  > a.size || a.data[i].size == 0)
        return NONE_PIXEL;

    return array_pixel_get(a.data[i], j);
}

bool array_array_pixel_set (struct array_array_pixel a, size_t i, size_t j, struct pixel value) {
    if (a.size == 0 || a.data[i].size == 0)
        return false;

    a.data[i].data[j] = value;
    return true;
}

void array_array_pixel_free (struct array_array_pixel* array) {
    for (size_t i = 0; i < array->size; ++i) {
        array_pixel_free(&array->data[i]);
    }
    free(array->data);
    free(array);
    array = NULL;
}

struct optional_array_array_pixel some_array_array_pixel (struct array_array_pixel* array) {
    return (struct optional_array_array_pixel) {.value = array, .valid = true};
}
