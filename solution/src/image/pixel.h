// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.

// This is a personal academic project. Dear PVS-Studio, please check it.
#ifndef BMP_IMAGE_ROTATOR_PIXEL_H
#define BMP_IMAGE_ROTATOR_PIXEL_H

#include "stdbool.h"
#include "stdint.h"

struct pixel {
    uint8_t b, g, r;
};

struct pixel_float {
    float b, g, r;
};

struct optional_pixel {
    bool valid;
    struct pixel value;
};

extern const struct optional_pixel NONE_PIXEL;
extern const struct pixel PIXEL_WHITE;

struct optional_pixel some_pixel (struct pixel i);

#endif //BMP_IMAGE_ROTATOR_PIXEL_H
