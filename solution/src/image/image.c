// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#include "image.h"
#include "malloc.h"

struct image* image_init (int32_t width, int32_t height) {
    //debug_print("Allocating memory for image with size %" PRIi32 "x%" PRIi32 "\n", width, height);
    struct image* image = malloc(sizeof (struct image));
    if (image) {
        //debug_print("Allocated successfully at %p\n", image);
        image->width  = width;
        image->height = height;
        struct optional_array_array_pixel opt_marray = array_array_pixel_init(height, width);
        if (opt_marray.valid) {
            image->data = opt_marray.value;
            return image;
        } else {
            free(image);
            return NULL;
        }
    }
    //debug_print("Malloc failure");

    return image;
}

enum image_status image_get_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel) {
    struct optional_pixel opt_pixel = array_array_pixel_get(*image->data, x, y);
    if (opt_pixel.valid) {
        *pixel = opt_pixel.value;
        return IMAGE_OK;
    } else {
        //debug_print("Invalid coordinates: x:%" PRIi32 " y:%" PRIi32 "\n");
        return IMAGE_INVALID_COORDINATES;
    }
}

enum image_status image_set_pixel_by_coordinates (struct image const* image, int32_t x, int32_t y, struct pixel* pixel) {
    if (array_array_pixel_set(*image->data, x, y, *pixel)) {
        return IMAGE_OK;
    } else {
        //debug_print("Invalid coordinates: x:%" PRIi32 " y:%" PRIi32 "\n");
        return IMAGE_INVALID_COORDINATES;
    }
}

struct image* image_copy(const struct image* image) {
    struct image* new_image = image_init(image->width, image->height);
    for (int x = 0; x < image->height; ++x) {
        for (int y = 0; y < image->width; ++y) {
            struct pixel pixel = {0};
            image_get_pixel_by_coordinates(image, x, y, &pixel);
            image_set_pixel_by_coordinates(new_image, x, y, &pixel);
        }
    }

    return new_image;
}

void image_destroy (struct image* image) {
    //debug_print("Deallocating image at address %p...\n", image);
    array_array_pixel_free(image->data);
    free(image);
}
