// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#ifndef BMP_IMAGE_ROTATOR_FILEIO_H
#define BMP_IMAGE_ROTATOR_FILEIO_H

#include <stdbool.h>
#include <stdio.h>

#define FILE_RB "rb"
#define FILE_WB "wb"

bool open_file (const char* filename, FILE** file, const char* mode);
bool close_file (FILE** file);

#endif //BMP_IMAGE_ROTATOR_FILEIO_H
