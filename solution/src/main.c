// Copyright (c) 2021 spynad (Danila Lysenko). All rights reserved.
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "format/bmp.h"
#include "file/fileio.h"
#include "transformation/transform_sepia.h"
#include "transformation/transform_rotate.h"

void error() {
    fprintf(stderr, "Usage: ./bmp_transform [--rotate || --sepia] <source> <dest>\n");
}

int main(int argc, char** argv) {
    if (argc != 4) {
        error();
        return 1;
    }

    struct rusage r;
    struct timeval start;
    struct timeval end;

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    FILE *in = NULL;
    FILE *out = NULL;

    struct image* image = NULL;
    struct image* new_image = NULL;


    if (open_file(argv[2], &in, FILE_RB)) {
        if (from_bmp(in, &image) == READ_OK) {
            if(!strcmp("--rotate", argv[1])) {
                new_image = rotate(*image);
            }
            else if(!strcmp("--sepia", argv[1])) {
                new_image = apply_sepia(image);
            }
            else if(!strcmp("--sepia-asm", argv[1])) {
                new_image = apply_sepia_asm(image);
            }
            else {
                error();
                return 1;
            }
        }
    }
    close_file(&in);
    if (open_file(argv[3], &out, FILE_WB)) {
        to_bmp(out, new_image);
        image_destroy(new_image);
        image = NULL;
        close_file(&out);
    }

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res = ((end.tv_sec - start.tv_sec) * 1000L) + (end.tv_usec - start.tv_usec) / 1000L;

    printf("took %ld ms to complete", res);

    return 0;

}
