
COMPILER = clang
COMPILER_FLAGS = -std=c17 -pedantic -Wall -Werror -ggdb -Wno-attributes -O2 -c
LINKER = ${COMPILER}
NASM = nasm
NASM_FLAGS = -felf64

SOURCE = solution/src
OBJ = obj
BUILD = build

all:
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/data_structures/array_pixel.c -o ${OBJ}/array_pixel.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/data_structures/marray_pixel.c -o ${OBJ}/marray_pixel.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/file/fileio.c -o ${OBJ}/fileio.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/format/bmp.c -o ${OBJ}/bmp.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/image/image.c -o ${OBJ}/image.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/image/pixel.c -o ${OBJ}/pixel.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/transformation/transform_rotate.c -o ${OBJ}/transform_rotate.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/transformation/transform_sepia.c -o ${OBJ}/transform_sepia.o
	${COMPILER} ${COMPILER_FLAGS} ${SOURCE}/main.c -o ${OBJ}/main.o
	${NASM} ${NASM_FLAGS} ${SOURCE}/transformation/transform_sepia.asm -o ${OBJ}/transform_sepia_asm.o
	${LINKER} -g obj/*.o -o ${BUILD}/main

clean:
	rm -rf build/*
	rm -rf obj/*
